class UserController < ApplicationController
  before_filter :authorize, :except => [:new]

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to login_path
    else
      render 'new'
    end
     
  end

  def edit
    @user = User.find(current_user.id)

  end

  def update
    @user = User.find(current_user.id)
   if !@user.update(user_params)
      render 'edit'
   end
  end

  # get article object from http params
  private 
  def user_params
    params.require(:user).permit(:name, :username, :email, :password_digest, :image)
  end

end

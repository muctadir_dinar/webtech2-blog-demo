class User < ActiveRecord::Base
  has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>", small:"48x48", tiny:"32x32"}, default_url: ":style/missing.png"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/

  has_many :comments


  EMAIL_REGEX = /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\Z/i
  validates :username, :presence => true, :uniqueness => true, :length => { :in => 3..20 }
  validates :email, :presence => true, :uniqueness => true, :format => EMAIL_REGEX
  validates :password_digest, :confirmation => true #password_confirmation attr
  validates_length_of :password_digest, :in => 6..20, :on => :create


   def authenticate (password)
     self.password_digest == password
   end

  has_secure_password
end

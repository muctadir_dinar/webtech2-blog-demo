# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create(name: 'Muctadir Hossain', username: 'muctadir', email: 'muctadir@gmail.com',  password_digest:'root123')
User.create(name: 'Bilal Saeed', username: 'bilal', email: 'bilal@gmail.com', password_digest:'asdfgh')
